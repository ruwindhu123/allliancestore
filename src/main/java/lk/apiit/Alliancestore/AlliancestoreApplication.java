package lk.apiit.Alliancestore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlliancestoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlliancestoreApplication.class, args);
	}
}
